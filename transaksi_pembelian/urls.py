from django.urls import path
from . import views

app_name = 'transaksi_pembelian'

urlpatterns = [
    path('daftar_transaksi_pembelian/', views.daftar, name='daftar'),
    path('update_transaksi_pembelian/<id_transaksi_pembelian>/<waktu_pembelian>/<total_pembayaran>', views.update, name='update'),
    path('create_transaksi_pembelian/', views.create, name='create'),
    path('profil_pengguna/', views.profile, name='profil'),
    path('delete_transaksi_pembelian/<id_transaksi_pembelian>', views.delete, name='delete'),
]