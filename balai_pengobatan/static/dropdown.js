$(document).ready(function(){
    $("#addDropdown").click(function(){
        $(".master > .form-group-dropdown:first").clone().appendTo(".added")
    })
    $("#deleteDropdown").click(function(){
        var cloned = $(".master > .form-group-dropdown:first").clone()
        $(".master").empty()
        $(".added").empty()
        $(".master").append(cloned)
    })
})