from django.shortcuts import render, redirect

import pandas as pd
from django.db import connection, utils
from balai_pengobatan.data import DataKeperluan
from collections import namedtuple
from .forms import UpdateBalaiForm, CreateBalaiForm, UpdateObatForm, CreateObatForm
from apotek.views import get_apotek_choices
from login_register.decorators import *

# Create your views here.
def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

@login_required
def balai(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM balai_pengobatan ORDER BY id_balai::int")
        rows = dictfetchall(cursor)
        cursor.execute("SELECT id_balai, nama_apotek FROM balai_apotek b, apotek a WHERE a.id_apotek = b.id_apotek")
        rows2 = dictfetchall(cursor)
        return render(request, 'balai_pengobatan/index.html', {'context': rows, 'context2':rows2})

@login_required
@admin_required
def delete_balai(request, id):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM balai_apotek WHERE id_balai = %s", [id])
        cursor.execute("DELETE FROM balai_pengobatan WHERE id_balai = %s", [id])
        return redirect("balai_pengobatan:index")

@login_required
@admin_required
def update_balai(request, id):
    with connection.cursor() as cursor:
        if request.method == "POST":
            form = UpdateBalaiForm(request.POST)
            print(form.is_valid())
            if form.is_valid():
                cd = form.cleaned_data
                raw = dict(form.data)
                id_balai = cd.get("id_balai")
                alamat_balai = cd.get("alamat_balai")
                nama_balai = cd.get("nama_balai")
                jenis_balai = cd.get("jenis_balai")
                telepon_balai = cd.get("telepon_balai")
                id_apotek_berasosiasi = form.cleaned_data["id_apotek_berasosiasi"]

                print(id_apotek_berasosiasi)

                query = "UPDATE balai_pengobatan SET id_balai = %s, "\
                        "alamat_balai = %s, nama_balai = %s, jenis_balai = %s, telepon_balai = %s"\
                        "WHERE id_balai = %s"
                
                cursor.execute(query, [id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai, id_balai])

                cursor.execute("DELETE FROM balai_apotek WHERE id_balai = %s", [id])
                query = "INSERT INTO balai_apotek(id_balai, id_apotek) "\
                        "VALUES (%s, %s)"
                
                for ii in range(len(id_apotek_berasosiasi)):
                    cursor.execute(query, [id_balai, id_apotek_berasosiasi[ii]])

                return redirect("balai_pengobatan:index")
        
        cursor.execute("SELECT * FROM BALAI_PENGOBATAN WHERE id_balai = %s", [id])
        row = dictfetchall(cursor)[0]
        row2 = get_apotek_choices()
        row3 = get_id_apotek_berasosiasi_choices(id)
        print(row)
        print(row2)
        return render(request, 'balai_pengobatan/update.html', {'context':row , 'range':range(1, len(row3)),
                    'tambah':row2, 'tambah2':row3})

@login_required
@admin_required
def create_balai(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = CreateBalaiForm(request.POST)
            print(request.POST)
            print(form.is_valid())
            if form.is_valid():
                cursor.execute("SELECT id_balai FROM balai_pengobatan ORDER BY id_balai::int desc LIMIT 1")
                id_balai = dictfetchall(cursor)[0]["id_balai"]
                id_balai_final = str(int(id_balai) + 1)
                cd = form.cleaned_data
                alamat_balai = cd.get("alamat_balai")
                nama_balai = cd.get("nama_balai")
                jenis_balai = cd.get("jenis_balai")
                telepon_balai = cd.get("telepon_balai")
                id_apotek_berasosiasi = form.cleaned_data["id_apotek_berasosiasi"]

                query = "INSERT INTO balai_pengobatan(id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai) "\
                        "VALUES (%s, %s, %s, %s, %s);"
                cursor.execute(query, [id_balai_final, alamat_balai, nama_balai, jenis_balai, telepon_balai])

                query = "INSERT INTO balai_apotek(id_balai, id_apotek) "\
                        "VALUES (%s, %s);"
                cursor.execute(query, [id_balai_final, id_apotek_berasosiasi])

                return redirect('balai_pengobatan:index')
        context = get_apotek_choices()
        return render(request, 'balai_pengobatan/create.html', {'tambah':context})

@login_required
def obat(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM obat ORDER BY id_obat::int")
        rows = dictfetchall(cursor)
        return render(request, 'obat/index.html', {'context': rows})

@login_required
@admin_required
def delete_obat(request, id):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM kandungan WHERE id_obat = %s", [id])
        cursor.execute("DELETE FROM obat WHERE id_obat = %s", [id])
        return redirect("balai_pengobatan:obat")

@login_required
@admin_required
def update_obat(request, id):
    with connection.cursor() as cursor:
        if request.method == "GET":
            cursor.execute("SELECT * FROM obat WHERE id_obat = %s", [id])
            context1 = dictfetchall(cursor)[0]
            context2 = get_id_merk_obat_choices()
            print(context2)
            return render(request, 'obat/update.html', {'context1':context1, 'context2':context2})
        elif request.method == "POST":
            form = UpdateObatForm(request.POST)
            print(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                id_obat = cd.get("id_obat")
                id_produk = cd.get("id_produk")
                id_merk_obat = cd.get("id_merk_obat")
                netto = cd.get("netto")
                dosis = cd.get("dosis")
                aturan_pakai = cd.get("aturan_pakai")
                kontraindikasi = cd.get("kontraindikasi")
                bentuk_kesediaan = cd.get("bentuk_kesediaan")

                query = "UPDATE obat SET id_obat = %s, id_produk = %s, id_merk_obat = %s, netto = %s, dosis = %s, "\
                        "aturan_pakai = %s, kontraindikasi = %s, bentuk_kesediaan = %s "\
                        "WHERE id_obat = %s"

                cursor.execute(query, [id_obat, id_produk, id_merk_obat, netto, dosis, aturan_pakai, kontraindikasi, bentuk_kesediaan, id_obat])
                return redirect('balai_pengobatan:obat')

@login_required
@admin_required
def create_obat(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = CreateObatForm(request.POST)
            print(request.POST)
            print(form.is_valid())
            if form.is_valid():
                cursor.execute("SELECT id_obat FROM obat ORDER BY id_obat::int desc LIMIT 1")
                id_obat = dictfetchall(cursor)[0]["id_obat"]
                id_obat_final = str(int(id_obat) + 1)
                cursor.execute("SELECT id_produk FROM produk ORDER BY id_produk::int desc LIMIT 1")
                id_produk = dictfetchall(cursor)[0]["id_produk"]
                id_produk_final = str(int(id_produk) + 1)
                cd = form.cleaned_data
                id_merk_obat = cd.get("id_merk_obat")
                netto = cd.get("netto")
                dosis = cd.get("dosis")
                aturan_pakai = cd.get("aturan_pakai")
                kontraindikasi = cd.get("kontraindikasi")
                bentuk_kesediaan = cd.get("bentuk_kesediaan")

                query = "INSERT INTO produk(id_produk) "\
                        "VALUES (%s);"
                
                cursor.execute(query, [id_obat_final])

                query = "INSERT INTO obat(id_obat, id_produk, id_merk_obat, netto, dosis, aturan_pakai, kontraindikasi, bentuk_kesediaan) "\
                        "VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"

                cursor.execute(query, [id_obat_final, id_produk_final, id_merk_obat, netto, dosis, aturan_pakai, 
                kontraindikasi, bentuk_kesediaan])

                return redirect('/balai_pengobatan/obat')
        
        context = get_id_merk_obat_choices()
        return render(request, 'obat/create.html', {'context':context})

def get_id_merk_obat_choices():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_merk_obat FROM merk_obat")
        id_merk_obat_cursor = dictfetchall(cursor)

        id_merk_obat = []

        for row in id_merk_obat_cursor:
            id_merk_obat.append(row['id_merk_obat'])

        return id_merk_obat

def get_id_apotek_berasosiasi_choices(id):
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_apotek FROM balai_apotek WHERE id_balai = %s", [id])
        id_apotek_cursor = dictfetchall(cursor)

        id_apotek = []

        for row in id_apotek_cursor:
            id_apotek.append(row['id_apotek'])

        return id_apotek