import pandas as pd
import os
from django.conf import settings

class DataProdukPengantaran:
    link_lpd = os.path.join(settings.BASE_DIR, 'data_csv/LIST_PRODUK_DIBELI.csv')
    link_pf = os.path.join(settings.BASE_DIR, 'data_csv/PENGANTARAN_FARMASI.csv')
    link_kurir = os.path.join(settings.BASE_DIR, 'data_csv/KURIR.csv')
    data_lpd = pd.read_csv(link_lpd).astype(str)
    data_pf = pd.read_csv(link_pf).astype(str)
    data_kurir = pd.read_csv(link_kurir).astype(str)