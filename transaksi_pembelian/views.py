from django.shortcuts import redirect, render

import os
from django.db import connection, utils
from collections import namedtuple
from .forms import createtr, updatetr
from tk_basdat_43.settings import STATIC_DIR
from .data import DataTransaksi
from login_register.decorators import *

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
    
@login_required
def daftar(request):
    dataset = []
    cursor = connection.cursor()
    email = request.session["email"]
    role = request.session["role"]

    if role == "KONSUMEN":
        sql = "SELECT * FROM FARMAKAMI.konsumen WHERE email ='"+ email +"';"
        cursor.execute(sql)
        dataset = namedtuplefetchall(cursor)
        id_konsumen = dataset[0].id_konsumen
        sql2 = "SELECT * FROM FARMAKAMI.TRANSAKSI_PEMBELIAN WHERE id_konsumen ='"+id_konsumen+"';"
        cursor.execute(sql2)
        dataset = namedtuplefetchall(cursor)
        return render(request, 'daftar_transaksi_pembelian.html', {"table":dataset})

    else:
        sql_daftar = "SELECT * FROM farmakami.TRANSAKSI_PEMBELIAN"
        cursor.execute(sql_daftar)
        dataset = namedtuplefetchall(cursor)
        return render(request, 'daftar_transaksi_pembelian.html', {"table":dataset})

@login_required
@admin_required
def update(request, id_transaksi_pembelian, waktu_pembelian, total_pembayaran):
    if request.method =='POST':
        update_form = request.POST["id_konsumen_select"]

        cursor = connection.cursor()

        if update_form != "none":
            sql = "update farmakami.transaksi_pembelian set id_konsumen = '" + update_form + "' where id_transaksi_pembelian = '" + id_transaksi_pembelian + "';"
            cursor.execute(sql)
            sql2 = "update farmakami.transaksi_pembelian set waktu_pembelian = NOW() where id_transaksi_pembelian = '" + id_transaksi_pembelian + "';"
            cursor.execute(sql2)

        return redirect('/transaksi_pembelian/daftar_transaksi_pembelian/')

    form = updatetr()
    cursor = connection.cursor()
    sql_idkonsumen = "select distinct id_konsumen from farmakami.konsumen order by id_konsumen ASC"
    cursor.execute(sql_idkonsumen)
    idkonsumen = namedtuplefetchall(cursor)
    args = {'form': form, 'id_transaksi_pembelian':id_transaksi_pembelian, 'waktu_pembelian':waktu_pembelian, 'total_pembayaran':total_pembayaran, 'idkonsumen':idkonsumen}
    return render(request, 'update_transaksi_pembelian.html', args)

@login_required
@admin_required
def create(request):
    context = {}
    if request.method =='POST':
        form = createtr(request.POST)
        context['form'] = form
        id_konsumen = request.POST["id_konsumen_select"]
        

        cursor = connection.cursor()

        temp = "select count(*) from farmakami.transaksi_pembelian"
        cursor.execute(temp)
        temp2 = cursor.fetchone()[0]

        temp3 = str(temp2)
        sql = "select count(*) from farmakami.transaksi_pembelian where id_transaksi_pembelian = '" + temp3 + "';"
        cursor.execute(sql)
        final = cursor.fetchone()[0]

        while(final != 0):
            temp2 = temp2 + 1
            temp3 = str(temp2)
            sql = "select count(*) from farmakami.transaksi_pembelian where id_transaksi_pembelian = '" + temp3 + "';"
            cursor.execute(sql)
            final = cursor.fetchone()[0]

        idtransaksi = "" + str(temp2)
        sql_create = "insert into farmakami.transaksi_pembelian VALUES (%s, NOW(), %s, %s);"
        cursor.execute(sql_create,(idtransaksi, 0, id_konsumen))
        return redirect('/transaksi_pembelian/daftar_transaksi_pembelian/')

    else:
        form = createtr()
    
    cursor = connection.cursor()
    sql_idkonsumen = "select distinct id_konsumen from farmakami.konsumen order by id_konsumen ASC"
    cursor.execute(sql_idkonsumen)
    idkonsumen = namedtuplefetchall(cursor)
    args = {'form': form, 'idkonsumen': idkonsumen}

    return render(request, 'create_transaksi_pembelian.html', args)

@login_required
@admin_required
def delete(request, id_transaksi_pembelian):
    cursor = connection.cursor()
    sql_delete = "delete from farmakami.TRANSAKSI_PEMBELIAN where id_transaksi_pembelian = '" + id_transaksi_pembelian + "'"
    cursor.execute(sql_delete)
    return redirect('/transaksi_pembelian/daftar_transaksi_pembelian/')

@login_required
def profile(request):
    email = request.session["email"]
    role = request.session["role"]
    cursor = connection.cursor()

    sql_pengguna = "SELECT * FROM FARMAKAMI.PENGGUNA WHERE email='"+email+"';"
    cursor.execute(sql_pengguna)
    data = namedtuplefetchall(cursor)
    pengguna = data[0]

    if role == "ADMIN_APOTEK":
        sql_admin = "SELECT * FROM FARMAKAMI.ADMIN_APOTEK WHERE email='"+email+"';"
        cursor.execute(sql_admin)
        data  = namedtuplefetchall(cursor)
        admin = data[0]
        return render(request, 'profil_pengguna.html', {"pengguna":pengguna, "admin":admin})

    elif role == "KONSUMEN":
        sql_konsumen = "SELECT * FROM FARMAKAMI.KONSUMEN WHERE email='"+email+"';"
        cursor.execute(sql_konsumen)
        data= namedtuplefetchall(cursor)
        konsumen = data[0]

        id_konsumen = konsumen.id_konsumen
        sql_alamat = "SELECT * FROM FARMAKAMI.ALAMAT_KONSUMEN WHERE id_konsumen='"+id_konsumen+"';"
        cursor.execute(sql_alamat)
        data = namedtuplefetchall(cursor)
        alamat = data
        return render(request, 'profil_pengguna.html', {"pengguna":pengguna, "konsumen":konsumen, "alamat":alamat})

    elif role == "CS":
        sql_cs = "SELECT * FROM FARMAKAMI.CS WHERE email='"+email+"';"
        cursor.execute(sql_cs)
        data  = namedtuplefetchall(cursor)
        cs = data[0]
        return render(request, 'profil_pengguna.html', {"pengguna":pengguna, "cs":cs})

    elif role == "KURIR":
        sql_kurir = "SELECT * FROM FARMAKAMI.KURIR WHERE email='"+email+"';"
        cursor.execute(sql_kurir)
        data  = namedtuplefetchall(cursor)
        kurir = data[0]
        return render(request, 'profil_pengguna.html', {"pengguna":pengguna, "kurir":kurir})

