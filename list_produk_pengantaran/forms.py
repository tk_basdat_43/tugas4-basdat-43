from django import forms

class CreateLPDForm(forms.Form):
    id_produk = forms.CharField(max_length=10)
    id_apotek = forms.CharField(max_length=10)
    id_transaksi = forms.CharField(max_length=10)
    jumlah = forms.IntegerField()

    def clean(self):
        cd = self.cleaned_data
        id_produk = cd.get("id_produk")
        id_apotek = cd.get("id_apotek")
        id_transaksi = cd.get("id_transaksi")
        jumlah = cd.get("jumlah")
        
        return cd

class UpdateLPDForm(forms.Form):
    id_transaksi_pembelian = forms.CharField(max_length=10)
    jumlah = forms.IntegerField()

    def clean(self):
        cd = self.cleaned_data
        id_transaksi_pembelian = cd.get("id_transaksi_pembelian")
        jumlah = cd.get("jumlah")
        
        return cd

class CreatePFForm(forms.Form):
    id_transaksi = forms.CharField(max_length=10)
    waktu = forms.CharField()
    biaya_kirim = forms.IntegerField()

    def clean(self):
        cd = self.cleaned_data
        id_transaksi = cd.get("id_transaksi")
        waktu = cd.get("waktu")
        biaya_kirim = cd.get("biaya_kirim")
        
        return cd

class UpdatePFForm(forms.Form):
    waktu = forms.CharField()
    status = forms.CharField(max_length=20)
    biaya_kirim = forms.IntegerField()

    def clean(self):
        cd = self.cleaned_data
        status = cd.get("status")
        waktu = cd.get("waktu")
        biaya_kirim = cd.get("biaya_kirim")
        
        return cd
