from django.shortcuts import redirect


def login_required(func):
    def wrapper(request, *args, **kwargs):
        if 'logged_in' not in request.session:
            return redirect("login_register:landingpage")
        else:
            return func(request, *args, **kwargs)
    return wrapper


def no_login(func):
    def wrapper(request, *args, **kwargs):
        if 'logged_in' in request.session:
            return redirect("login_register:homepage")
        else:
            return func(request, *args, **kwargs)
    return wrapper


def admin_required(func):
    def wrapper(request, *args, **kwargs):
        if request.session["role"] == "ADMIN_APOTEK":
            return func(request, *args, **kwargs)
        else:
            return redirect("login_register:homepage")
    return wrapper

def apoteker_required(func):
    apoteker = ["ADMIN_APOTEK", "CS"]

    def wrapper(request, *args, **kwargs):
        if request.session["role"] in apoteker:
            return func(request, *args, **kwargs)
        else:
            return redirect("login_register:homepage")

    return wrapper

def konsumen_required(func):
    def wrapper(request, *args, **kwargs):
        if request.session["role"] == "KONSUMEN":
            return func(request, *args, **kwargs)
        else:
            return redirect("login_register:homepage")
    return wrapper
