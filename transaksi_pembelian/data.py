import pandas as pd
import os
from django.conf import settings

class DataTransaksi:
    link_transaksi_pembelian = os.path.join(settings.BASE_DIR,'data_csv/TRANSAKSI_PEMBELIAN.csv')
    data_transaksi = pd.read_csv(link_transaksi_pembelian).astype(str)
    