$(document).ready(function () {
    url_split = window.location.pathname.split("/").filter((a) => a != "")
    id_apotek_url = url_split[url_split.length - 2]
    id_produk_url = url_split[url_split.length - 1]

    $('#id_apotek').on('change', function() {
      get_id_produk_choices($(this).val())
    });
})

function get_id_produk_choices(id_apotek){
    $.ajax({
        method: 'GET',
        url: '/apotek/produk/avlble/' + id_apotek,
        success: function(response){

            if(id_apotek == id_apotek_url){
                response.push(id_produk_url)
                response = response.sort((a,b) => Number(a) - Number(b))
            }

            $('#id_produk').empty()
            for (ii = 0; ii < response.length; ii++){
                id_produk = response[ii]
                if(id_produk == id_produk_url){
                    html = "<option value="+ id_produk  + " selected>"+ id_produk +"</option>"
                    $("#id_produk").append(html)
                }
                else {
                    html = "<option value="+ id_produk  + " >"+ id_produk +"</option>"
                    $("#id_produk").append(html)
                }
            }

        }
    })
}