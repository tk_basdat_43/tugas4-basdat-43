from django.shortcuts import render, redirect
from django.db import connection, utils
from django.http import JsonResponse
from .forms import CreateLPDForm, UpdateLPDForm, CreatePFForm, UpdatePFForm
from login_register.decorators import *
import datetime

# Create your views here.
def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

@login_required
def list_produk_dibeli(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM list_produk_dibeli")
        rows = dictfetchall(cursor)
        return render(request, 'list_produk_pengantaran/list_produk_dibeli.html', {'content': rows})

@login_required
@admin_required
def update_list_produk_dibeli(request, id_apotek, id_produk, id_transaksi_pembelian):
    with connection.cursor() as cursor:
        list_id_transaksi_pembelian = get_id_transaksi_pembelian_choices()

        if request.method == "GET":
            list_id_transaksi_pembelian = get_id_transaksi_pembelian_choices()
            cursor.execute("SELECT * FROM list_produk_dibeli WHERE id_apotek = %s AND id_produk = %s AND id_transaksi_pembelian = %s"
                           , [id_apotek, id_produk, id_transaksi_pembelian])
            row = dictfetchall(cursor)[0]
            return render(request, 'list_produk_pengantaran/update_list_produk_dibeli.html',
                           {'data': row, 'list_id_transaksi_pembelian':list_id_transaksi_pembelian})

        elif request.method == "POST":
            form = UpdateLPDForm(request.POST)
            data = dict(map(lambda kv: (kv[0], kv[1][0]), dict(request.POST).items()))
            if form.is_valid():
                cd = form.cleaned_data
                id_transaksi_pembelian_new = cd.get("id_transaksi_pembelian")
                jumlah = cd.get("jumlah")
                
                query = "UPDATE list_produk_dibeli SET id_transaksi_pembelian = %s, " \
                        "jumlah = %s WHERE id_apotek = %s AND id_produk = %s AND id_transaksi_pembelian = %s"

                try:
                    cursor.execute(query, [id_transaksi_pembelian_new, jumlah,
                                           id_apotek, id_produk, id_transaksi_pembelian])

                except utils.IntegrityError as e:
                    return render(request, 'list_produk_pengantaran/update_list_produk_dibeli.html', {"data": data,
                                    'list_id_transaksi_pembelian':list_id_transaksi_pembelian, "message" : str(e)})
                return redirect("list_produk_pengantaran:list_produk_dibeli")

            return render(request, 'list_produk_pengantaran/update_list_produk_dibeli.html',
                          {"data": data, 'list_id_transaksi_pembelian':list_id_transaksi_pembelian, "form" : form})

@login_required
@admin_required
def delete_list_produk_dibeli(request, id_apotek, id_produk, id_transaksi_pembelian):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM list_produk_dibeli WHERE id_apotek = %s AND id_produk = %s AND id_transaksi_pembelian = %s", [id_apotek, id_produk, id_transaksi_pembelian])
        return redirect('list_produk_pengantaran:list_produk_dibeli')

@login_required
@admin_required
def create_list_produk_dibeli(request):
    with connection.cursor() as cursor:
        list_id_produk = get_produk_choices()
        list_id_transaksi = get_id_transaksi_pembelian_choices()

        if request.method == "GET":
            list_id_apotek = id_apotek_choices_raw(list_id_produk[0])
            return render(request, "list_produk_pengantaran/create_list_produk_dibeli.html",
                          {"list_id_apotek": list_id_apotek,
                           "list_id_produk": list_id_produk, 'list_id_transaksi': list_id_transaksi})

        elif request.method == "POST":
            form = CreateLPDForm(request.POST)
            data = dict(map(lambda kv: (kv[0], kv[1][0]), dict(request.POST).items()))
            list_id_apotek = id_apotek_choices_raw(data["id_produk"])

            if form.is_valid():
                cd = form.cleaned_data
                id_produk = cd.get("id_produk")
                id_apotek = cd.get("id_apotek")
                id_transaksi = cd.get("id_transaksi")
                jumlah = cd.get("jumlah")

                query = "INSERT INTO list_produk_dibeli(id_apotek, id_produk, id_transaksi_pembelian, jumlah) " \
                        "VALUES(%s, %s, %s, %s);"
                try:
                    cursor.execute(query, [id_apotek, id_produk, id_transaksi, jumlah])

                except utils.IntegrityError as e:
                    return render(request, 'list_produk_pengantaran/create_list_produk_dibeli.html',
                                  {"data": data, "list_id_apotek": list_id_apotek,
                                   "list_id_produk": list_id_produk, 'list_id_transaksi': list_id_transaksi,
                                   "message": str(e)})

                return redirect("list_produk_pengantaran:list_produk_dibeli")

            return render(request, "list_produk_pengantaran/create_list_produk_dibeli.html",
                          {"data": data, 'form': form,
                           "list_id_apotek": list_id_apotek, 'list_id_transaksi': list_id_transaksi,
                           "list_id_produk": list_id_produk})

@login_required
def pengantaran_farmasi(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM pengantaran_farmasi")
        rows = dictfetchall(cursor)
        return render(request, 'list_produk_pengantaran/pengantaran_farmasi.html', {'content': rows})

@login_required
@admin_required
def update_pengantaran_farmasi(request, id):
    with connection.cursor() as cursor:
        if request.method == "GET":
            cursor.execute("SELECT * FROM pengantaran_farmasi WHERE id_pengantaran = %s", [id])
            row = dictfetchall(cursor)[0]
            return render(request, 'list_produk_pengantaran/update_pengantaran_farmasi.html', {"data": row})
        elif request.method == "POST":
            form = UpdatePFForm(request.POST)
            data = dict(map(lambda kv: (kv[0], kv[1][0]), dict(request.POST).items()))
            if form.is_valid():
                cd = form.cleaned_data
                waktu = cd.get("waktu")
                status = cd.get("status")
                biaya_kirim = cd.get("biaya_kirim")

                now = datetime.datetime.now().strftime('%H:%M')
                waktu_db = str(waktu) + " " + now
                
                query = "UPDATE pengantaran_farmasi SET waktu = %s, "\
                        "status_pengantaran = %s, biaya_kirim = %s WHERE id_pengantaran = %s"

                try:
                    cursor.execute(query, [waktu_db, status, biaya_kirim, id])
                except utils.IntegrityError as e:
                    return render(request, 'list_produk_pengantaran/update_pengantaran_farmasi.html', {"data": data,
                                                                         "message" : str(e)})

                return redirect("list_produk_pengantaran:pengantaran_farmasi")

            return render(request, 'list_produk_pengantaran/update_pengantaran_farmasi.html', {"data": data, "form" : form})

@login_required
@admin_required
def delete_pengantaran_farmasi(request, id):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM pengantaran_farmasi WHERE id_pengantaran = %s", [id])
        return redirect('list_produk_pengantaran:pengantaran_farmasi')

@login_required
@admin_required
def create_pengantaran_farmasi(request):
    with connection.cursor() as cursor:
        list_id_transaksi = get_id_transaksi_pengantaran()
        if request.method == "GET":
            return render(request, "list_produk_pengantaran/create_pengantaran_farmasi.html",
                            {'list_id_transaksi': list_id_transaksi})
        elif request.method == "POST":
            form = CreatePFForm(request.POST)
            if form.is_valid():
                cursor.execute("SELECT id_pengantaran::INTEGER FROM pengantaran_farmasi ORDER BY id_pengantaran desc LIMIT 1")
                id_pengantaran = dictfetchall(cursor)[0]["id_pengantaran"]
                id_pengantaran = str(int(id_pengantaran) + 1)

                cd = form.cleaned_data
                id_transaksi = cd.get("id_transaksi")
                waktu = cd.get("waktu")
                biaya_kirim = cd.get("biaya_kirim")

                now = datetime.datetime.now().strftime('%H:%M')
                waktu_db = str(waktu) + " " + now
                
                query = "INSERT INTO pengantaran_farmasi(id_pengantaran,id_kurir,id_transaksi_pembelian," \
                        "waktu,status_pengantaran,biaya_kirim, total_biaya) " \
                        "VALUES(%s, '2', %s, %s, 'Picked Up', %s, '0');"

                try:
                    cursor.execute(query, [id_pengantaran, id_transaksi, waktu_db, biaya_kirim])
                except utils.IntegrityError as e:
                    return render(request, "list_produk_pengantaran/create_pengantaran_farmasi.html", 
                                    {"message" : str(e), 'list_id_transaksi': list_id_transaksi})

                return redirect("list_produk_pengantaran:pengantaran_farmasi")

            return render(request, "list_produk_pengantaran/create_pengantaran_farmasi.html", 
                            {"form" : form, 'list_id_transaksi': list_id_transaksi})

def get_id_transaksi_pembelian_choices():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_transaksi_pembelian FROM transaksi_pembelian")
        transaksi_cursor = dictfetchall(cursor)

        id_transaksi_pembelian = []

        for row in transaksi_cursor:
            id_transaksi_pembelian.append(row['id_transaksi_pembelian'])

        return id_transaksi_pembelian

def get_produk_choices():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_produk FROM produk")
        produk_cursor = dictfetchall(cursor)

        produk = []

        for row in produk_cursor:
            produk.append(row['id_produk'])

        return produk

def id_apotek_choices_raw(id):
    with connection.cursor() as cursor:
        query = "SELECT DISTINCT id_apotek FROM produk_apotek WHERE id_produk = %s"
        cursor.execute(query, [id])

        list_id_apotek = []
        rows = dictfetchall(cursor)

        for row in rows:
            list_id_apotek.append(row['id_apotek'])

        return list_id_apotek

def get_id_transaksi_pengantaran():
    with connection.cursor() as cursor:
        query = "SELECT id_transaksi_pembelian FROM transaksi_pembelian WHERE id_transaksi_pembelian NOT IN" \
            "(SELECT id_transaksi_pembelian FROM pengantaran_farmasi)"
        cursor.execute(query)

        list_id_transaksi = []
        rows = dictfetchall(cursor)

        for row in rows:
            list_id_transaksi.append(row['id_transaksi_pembelian'])

        return list_id_transaksi

def id_apotek_choices(request, id):
    if request.method == "GET":
        return JsonResponse(id_apotek_choices_raw(id), safe= False)