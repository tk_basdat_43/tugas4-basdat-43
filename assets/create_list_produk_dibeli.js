$(document).ready(function () {
    url_split = window.location.pathname.split("/").filter((a) => a != "")
    id_produk_url = url_split[url_split.length - 2]

    $('#id_produk').on('change', function() {
      get_id_apotek_choices($(this).val())
    });
})

function get_id_apotek_choices(id_produk){
    $.ajax({
        method: 'GET',
        url: '/list_produk_pengantaran/apotek/avlble/' + id_produk,
        success: function(response){

            $('#id_apotek').empty()
            for (ii = 0; ii < response.length; ii++){
                id_apotek = response[ii]
                html = "<option value="+ id_apotek  + " >"+ id_apotek +"</option>"
                $("#id_apotek").append(html)

            }

        }
    })
}