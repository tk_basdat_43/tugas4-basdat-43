from django import forms
import re


class LoginForm(forms.Form):
    username = forms.EmailField(label='Username:', max_length=50)
    password = forms.CharField(label='Password:', max_length=200, widget=forms.PasswordInput)


class AdminApotekForm(forms.Form):
    email = forms.EmailField(label='Email', max_length=50)
    password = forms.CharField(label='Password', max_length=200, widget=forms.PasswordInput)
    nama_lengkap = forms.CharField(label='Nama Lengkap', max_length=50)
    no_telepon = forms.CharField(label='No Telepon', max_length=20)

    def clean(self):
        cd = self.cleaned_data
        email = cd.get("email")
        password = cd.get("password")
        nama_lengkap = cd.get("nama_lengkap")
        no_telepon = cd.get("no_telepon")

        if not bool(re.search('^\d{3} \d{3} \d{4}$', str(no_telepon))):
            self.add_error("no_telepon", "Wrong No Telp Apotek Format {XXX XXX XXXX}")

        return cd


class KurirForm(AdminApotekForm):
    nama_perusahaan = forms.CharField(label='Nama Perusahaan', max_length=50)


class KonsumenForm(AdminApotekForm):
    jenis_kelamin = forms.CharField(label='Jenis Kelamin', max_length=1)
    tanggal_lahir = forms.DateField(label='Waktu', widget=forms.DateInput(attrs={'type':'date'}))

    def clean(self):
        super().clean()
        cd = self.cleaned_data
        jenis_kelamin = cd.get("jenis_kelamin")
        tanggal_lahir = cd.get("tanggal_lahir")

        if jenis_kelamin != 'M' and jenis_kelamin != 'F':
            self.add_error("jenis_kelamin", "Jenis Kelamin F/M")

        return cd


class CSForm(AdminApotekForm):
    no_ktp = forms.CharField(label='No KTP', max_length=50)
    no_sia = forms.CharField(label='No SIA', max_length=50)

    def clean(self):
        super().clean()

        cd = self.cleaned_data
        no_ktp = cd.get("no_ktp")
        no_sia = cd.get("no_sia")

        if re.search('[\D]', str(no_sia)):
            self.add_error('no_sia', "No SIA contains Non-Digit character")

        if re.search('[\D]', str(no_ktp)):
            self.add_error('no_ktp', "No KTP contains Non-Digit character")

        return cd


class AlamatForm(forms.Form):
    status_alamat = forms.CharField(label='Alamat', required=False, widget=forms.TextInput(attrs={'placeholder': 'Status Alamat'}))
    alamat = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={'placeholder': 'Alamat'}))

    def clean(self):
        raw = dict(self.data)
        alamat = raw["alamat"]
        status_alamat = raw["status_alamat"]

        if len(set(alamat)) != len(alamat):
            self.add_error("alamat", "Duplicate Alamat")

        if "" in alamat:
            self.add_error("alamat", "Null Alamat Input")

        if "" in status_alamat:
            self.add_error("status_alamat", "Null Status Alamat Input")

        self.cleaned_data["alamat"] = alamat
        self.cleaned_data["status_alamat"] = status_alamat

        return self.cleaned_data




