from django.urls import path
from . import views


app_name = 'balai_pengobatan'

urlpatterns = [
    path('', views.balai, name="index"),
    path('create_balai', views.create_balai, name="create_balai"),
    path('delete_balai/<id>', views.delete_balai, name ='delete_balai'),
    path('update_balai/<id>', views.update_balai, name="update_balai"),
    path('obat', views.obat, name="obat"),
    path('obat/create_obat', views.create_obat, name="create_obat"),
    path('obat/update_obat/<id>', views.update_obat, name="update_obat"),
    path('obat/delete_obat/<id>', views.delete_obat, name="delete_obat")
]