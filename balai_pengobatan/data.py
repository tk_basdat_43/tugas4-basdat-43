import pandas as pd

class DataKeperluan:
    data_balai_pengobatan = pd.read_csv('./data_csv/BALAI_PENGOBATAN.csv')\
        .astype({"id_balai":str, "alamat_balai":str, "telepon_balai":str})
    data_obat= pd.read_csv("./data_csv/OBAT.csv")\
        .astype({"id_obat" : str, "id_produk":str, "id_merk_obat":str})
    data_apotek = pd.read_csv("./data_csv/APOTEK.csv").astype(str)
    data_merk_obat = pd.read_csv("./data_csv/MERK_OBAT.csv").astype(str)