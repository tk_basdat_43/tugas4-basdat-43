from django import forms
import re

class UpdateApotekForm(forms.Form):
    id_apotek = forms.CharField(max_length=10, required=True)
    nama_apotek = forms.CharField(max_length=50, required=True)
    alamat_apotek = forms.CharField(required=True)
    telepon_apotek = forms.CharField(max_length=20, required=True)
    nama_penyelenggara = forms.CharField(max_length=50, required=True)
    no_sia = forms.CharField(max_length=20, required=True)
    email = forms.EmailField(max_length=50, required=True)

    def clean(self):
        cd = self.cleaned_data
        id_apotek = cd.get("id_apotek")
        nama_apotek = cd.get("nama_apotek")
        alamat_apotek = cd.get("alamat_apotek")
        telepon_apotek = cd.get("telepon_apotek")
        nama_penyelenggara = cd.get("nama_penyelenggara")
        no_sia = cd.get("no_sia")
        email = cd.get("email")

        if re.search('[\D]', str(id_apotek)):
            self.add_error("id_apotek", "id_apotek contains Non-Digit character")

        if re.search('[\D]', str(no_sia)):
            self.add_error('no_sia', "No SIA contains Non-Digit character")

        if not bool(re.search('^\d{3} \d{3} \d{4}$', str(telepon_apotek))):
            self.add_error("telepon_apotek", "Wrong No Telp Apotek Format {XXX - XXX - XXXX}")

        return cd


class CreateApotekForm(forms.Form):
    nama_apotek = forms.CharField(max_length=50, required=True)
    alamat_apotek = forms.CharField(required=True)
    telepon_apotek = forms.CharField(max_length=20, required=True)
    nama_penyelenggara = forms.CharField(max_length=50, required=True)
    no_sia = forms.CharField(max_length=20, required=True)
    email = forms.EmailField(max_length=50, required=True)

    def clean(self):
        cd = self.cleaned_data
        nama_apotek = cd.get("nama_apotek")
        alamat_apotek = cd.get("alamat_apotek")
        telepon_apotek = cd.get("telepon_apotek")
        nama_penyelenggara = cd.get("nama_penyelenggara")
        no_sia = cd.get("no_sia")
        email = cd.get("email")

        if re.search('[\D]', str(no_sia)):
            self.add_error('no_sia', "No SIA contains Non-Digit character")

        if not bool(re.search('^\d{3} \d{3} \d{4}$', str(telepon_apotek))):
            self.add_error("telepon_apotek", "Wrong No Telp Apotek Format {XXX XXX XXXX}")

        return cd


class ProdukApotekForm(forms.Form):
    id_apotek = forms.CharField(max_length=10)
    id_produk = forms.CharField(max_length=10)
    harga_jual = forms.IntegerField()
    satuan_penjualan = forms.CharField(max_length=5)
    stok = forms.IntegerField()

    def clean(self):
        cd = self.cleaned_data
        id_apotek = cd.get("id_apotek")
        id_produk = cd.get("id_produk")
        harga_jual = cd.get("harga_jual")
        satuan_penjualan = cd.get("satuan_penjualan")
        stok = cd.get("stok")

        if re.search('[\D]', str(id_apotek)):
            self.add_error("id_apotek", "id_apotek contains Non-Digit character")

        if re.search('[\D]', str(id_produk)):
            self.add_error("id_produk", "id_produk contains Non-Digit character")

        if harga_jual < 0:
            self.add_error("harga_jual", "Harga Jual < 0")

        if stok < 0:
            self.add_error("harga_jual", "Stok < 0")

        return cd

