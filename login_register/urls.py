from django.urls import path
from . import views

app_name = 'login_register'

urlpatterns = [
    path('', views.home, name='landingpage'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('homepage/', views.homepage, name='homepage'),
    path('register/kurir/', views.kurir, name='kurir'),
    path('register/konsumen/', views.konsumen, name='konsumen'),
    path('register/admin-apotek/', views.admin_apotek, name='admin_apotek'),
    path('register/cs/', views.cs, name='cs'),
    path('logout/', views.logout, name="logout")
]