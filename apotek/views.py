from django.shortcuts import render, redirect
from django.db import connection, utils
from django.http import JsonResponse
from .forms import *
from login_register.decorators import *

# Create your views here.


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


@login_required
def list_apotek(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM apotek ORDER BY id_apotek::numeric")
        rows = dictfetchall(cursor)
        return render(request, 'apotek/list_apotek.html', {'content': rows})


@login_required
def list_produk_apotek(request):
    # content_produk_apotek = DataApotek.data_produk_apotek.to_dict('records')
    # return render(request, 'apotek/list_produk_apotek.html', {"content":content_produk_apotek})
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM produk_apotek ORDER BY id_produk::numeric , id_apotek::numeric ")
        rows = dictfetchall(cursor)
        return render(request, 'apotek/list_produk_apotek.html', {'content': rows})


@login_required
@admin_required
def delete_apotek(request, id):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM apotek WHERE id_apotek = %s", [id])
        return redirect('apotek:list_apotek')


@login_required
@admin_required
def delete_produk_apotek(request, id_apotek, id_produk):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM produk_apotek WHERE id_apotek = %s AND id_produk = %s", [id_apotek, id_produk])
        return redirect('apotek:list_produk_apotek')


@login_required
@admin_required
def update_apotek(request, id):
    with connection.cursor() as cursor:
        if request.method == "GET":
            cursor.execute("SELECT * FROM apotek WHERE id_apotek = %s", [id])
            row = dictfetchall(cursor)[0]
            print(row)
            return render(request, 'apotek/update_apotek.html', {"data": row})
        elif request.method == "POST":
            form = UpdateApotekForm(request.POST)
            print(request.POST)
            data = dict(map(lambda kv: (kv[0], kv[1][0]), dict(request.POST).items()))
            if form.is_valid():
                cd = form.cleaned_data
                id_apotek = cd.get("id_apotek")
                nama_apotek = cd.get("nama_apotek")
                alamat_apotek = cd.get("alamat_apotek")
                telepon_apotek = cd.get("telepon_apotek")
                nama_penyelenggara = cd.get("nama_penyelenggara")
                no_sia = cd.get("no_sia")
                email = cd.get("email")


                query = "UPDATE apotek SET nama_apotek = %s, "\
                        "alamat_apotek = %s, telepon_apotek = %s, nama_penyelenggara = %s, "\
                        "no_sia = %s, email = %s WHERE id_apotek = %s"

                try:
                    cursor.execute(query, [nama_apotek, alamat_apotek,telepon_apotek,
                                           nama_penyelenggara, no_sia, email, id_apotek])
                except utils.IntegrityError as e:
                    return render(request, 'apotek/update_apotek.html', {"data": data,
                                                                         "message" : str(e)})

                return redirect("apotek:list_apotek")

            return render(request, 'apotek/update_apotek.html', {"data": data, "form" : form})


@login_required
@admin_required
def update_produk_apotek(request, id_apotek, id_produk):
    with connection.cursor() as cursor:
        list_id_apotek = get_apotek_choices()
        list_id_produk = id_produk_choices_raw(id_apotek)
        list_id_produk.append(id_produk)
        list_id_produk.sort(key=lambda x: int(x))

        if request.method == "GET":
            list_id_apotek = get_apotek_choices()
            list_id_produk = id_produk_choices_raw(id_apotek)
            list_id_produk.append(id_produk)
            list_id_produk.sort(key= lambda x : int(x))
            cursor.execute("SELECT * FROM produk_apotek WHERE id_apotek = %s AND id_produk = %s"
                           , [id_apotek, id_produk])
            row = dictfetchall(cursor)[0]
            return render(request, 'apotek/update_produk_apotek.html',
                           {"data": row, "list_id_apotek": list_id_apotek,
                            "list_id_produk": list_id_produk})

        elif request.method == "POST":
            form = ProdukApotekForm(request.POST)
            data = dict(map(lambda kv: (kv[0], kv[1][0]), dict(request.POST).items()))
            if form.is_valid():
                cd = form.cleaned_data
                id_apotek_new = cd.get("id_apotek")
                id_produk_new = cd.get("id_produk")
                harga_jual = cd.get("harga_jual")
                satuan_penjualan = cd.get("satuan_penjualan")
                stok = cd.get("stok")

                query = "UPDATE produk_apotek SET id_apotek = %s, " \
                        "id_produk = %s, harga_jual = %s, satuan_penjualan = %s, " \
                        "stok = %s WHERE id_apotek = %s AND id_produk = %s"
                try:
                    cursor.execute(query, [id_apotek_new, id_produk_new, harga_jual,
                                           satuan_penjualan, stok, id_apotek, id_produk])

                except utils.IntegrityError as e:
                    return render(request, 'apotek/update_apotek.html', {"data": data,
                                                                         "message" : str(e)})
                return redirect("apotek:list_produk_apotek")

            return render(request, 'apotek/update_produk_apotek.html',
                          {"data": data, "list_id_apotek": list_id_apotek,
                           "list_id_produk": list_id_produk, "form" : form})


@login_required
@admin_required
def create_apotek(request):
    with connection.cursor() as cursor:
        if request.method == "GET":
            return render(request, "apotek/create_apotek.html")
        elif request.method == "POST":
            form = CreateApotekForm(request.POST)
            if form.is_valid():
                cursor.execute("SELECT id_apotek FROM apotek ORDER BY id_apotek::numeric DESC LIMIT 1")
                id_apotek = dictfetchall(cursor)[0]["id_apotek"]
                id_apotek = str(int(id_apotek) + 1)

                cd = form.cleaned_data
                nama_apotek = cd.get("nama_apotek")
                alamat_apotek = cd.get("alamat_apotek")
                telepon_apotek = cd.get("telepon_apotek")
                nama_penyelenggara = cd.get("nama_penyelenggara")
                no_sia = cd.get("no_sia")
                email = cd.get("email")

                query = "INSERT INTO apotek(id_apotek, email, no_sia, " \
                        "nama_penyelenggara, nama_apotek, alamat_apotek, telepon_apotek) " \
                        "VALUES(%s, %s, %s, %s, %s, %s, %s);"

                try:
                    cursor.execute(query, [id_apotek, email,
                                           no_sia, nama_penyelenggara, nama_apotek,
                                           alamat_apotek, telepon_apotek])
                except utils.IntegrityError as e:
                    return render(request, "apotek/create_apotek.html", {"message" : str(e)})

                return redirect("apotek:list_apotek")

            return render(request, "apotek/create_apotek.html", {"form" : form})


@login_required
@admin_required
def create_produk_apotek(request):
    with connection.cursor() as cursor:
        list_id_apotek = get_apotek_choices()

        if request.method == "GET":
            list_id_produk = id_produk_choices_raw(list_id_apotek[0])
            return render(request, "apotek/create_produk_apotek.html",
                          {"list_id_apotek": list_id_apotek,
                           "list_id_produk": list_id_produk})

        elif request.method == "POST":
            form = ProdukApotekForm(request.POST)
            data = dict(map(lambda kv: (kv[0], kv[1][0]), dict(request.POST).items()))
            list_id_produk = id_produk_choices_raw(data["id_apotek"])

            if form.is_valid():
                cd = form.cleaned_data
                id_apotek = cd.get("id_apotek")
                id_produk = cd.get("id_produk")
                harga_jual = cd.get("harga_jual")
                satuan_penjualan = cd.get("satuan_penjualan")
                stok = cd.get("stok")

                query = "INSERT INTO produk_apotek(harga_jual, stok, satuan_penjualan, " \
                        "id_produk, id_apotek) " \
                        "VALUES(%s, %s, %s, %s, %s);"
                try:
                    cursor.execute(query, [harga_jual, stok,satuan_penjualan,
                                           id_produk, id_apotek])

                except utils.IntegrityError as e:
                    return render(request, 'apotek/create_produk_apotek.html',
                                  {"data": data, "list_id_apotek": list_id_apotek,
                                   "list_id_produk": list_id_produk,
                                   "message": str(e)})

                return redirect("apotek:list_produk_apotek")

            return render(request, "apotek/create_produk_apotek.html",
                          {"data": data, 'form': form,
                           "list_id_apotek": list_id_apotek,
                           "list_id_produk": list_id_produk})


def get_apotek_choices():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_apotek FROM apotek ORDER BY id_apotek::numeric ")
        apotek_cursor = dictfetchall(cursor)

        apotek = []

        for row in apotek_cursor:
            apotek.append(row['id_apotek'])

        return apotek


def id_produk_choices_raw(id):
    with connection.cursor() as cursor:
        query = "SELECT id_produk FROM produk WHERE id_produk NOT IN (" \
                "SELECT id_produk FROM produk_apotek WHERE id_apotek = %s)" \
                " ORDER BY id_produk::numeric"
        cursor.execute(query, [id])

        list_id_produk = []
        rows = dictfetchall(cursor)

        for row in rows:
            list_id_produk.append(row['id_produk'])

        return list_id_produk


def id_produk_choices(request, id):
    if request.method == "GET":
        return JsonResponse(id_produk_choices_raw(id), safe= False)




