from django import forms
import re

idbalai = [
    ('1',"1"),
    ('2',"2"),
    ('3',"3"),
    ('4',"4"),
    ('5',"5"),
]

jenisbalai = [
    ('1',"Rumah Sakit"),
    ('2',"Praktek Pribadi"),
    ('3',"Klinik"),
]

class UpdateBalaiForm(forms.Form):
    id_balai = forms.CharField(max_length=10, required=True)
    alamat_balai = forms.CharField(required=True)
    nama_balai = forms.CharField(max_length=50, required=True)
    jenis_balai = forms.CharField(max_length=50, required=True)
    telepon_balai = forms.CharField(max_length=15)
    id_apotek_berasosiasi = forms.CharField(max_length=15)

    def clean(self):
        cd = self.cleaned_data
        raw = dict(self.data)
        id_balai = cd.get("id_balai")
        alamat_balai = cd.get("alamat_balai")
        nama_balai = cd.get("nama_balai")
        jenis_balai = cd.get("jenis_balai")
        telepon_balai = cd.get("telepon_balai")
        id_apotek_berasosiasi = raw["id_apotek_berasosiasi"]
        cd["id_apotek_berasosiasi"] = id_apotek_berasosiasi
        
        return cd

class CreateBalaiForm(forms.Form):
    alamat_balai = forms.CharField(required=True)
    nama_balai = forms.CharField(max_length=50, required=True)
    jenis_balai = forms.CharField(max_length=50, required=True)
    telepon_balai = forms.CharField(max_length=15, required=False)
    id_apotek_berasosiasi = forms.CharField(max_length=15)

    def clean(self):
        cd = self.cleaned_data
        raw = dict(self.data)
        alamat_balai = cd.get("alamat_balai")
        nama_balai = cd.get("nama_balai")
        jenis_balai = cd.get("jenis_balai")
        telepon_balai = cd.get("telepon_balai")
        id_apotek_berasosiasi = raw["id_apotek_berasosiasi"]
        cd["id_apotek_berasosiasi"] = id_apotek_berasosiasi
        
        return cd

class UpdateObatForm(forms.Form):
    id_obat = forms.CharField(required=True)
    id_produk = forms.CharField(required=True)
    id_merk_obat = forms.CharField(required=True)
    netto = forms.CharField(required=True)
    dosis = forms.CharField(required=True)
    aturan_pakai = forms.CharField(required=True)
    kontraindikasi = forms.CharField(required=True)
    bentuk_kesediaan = forms.CharField(required=True)

    def clean(self):
        cd = self.cleaned_data
        id_obat = cd.get("id_obat")
        id_produk = cd.get("id_produk")
        id_merk_obat = cd.get("id_merk_obat")
        netto = cd.get("netto")
        dosis = cd.get("dosis")
        aturan_pakai = cd.get("aturan_pakai")
        kontraindikasi = cd.get("kontraindikasi")
        bentuk_kesediaan = cd.get("bentuk_kesediaan")

        return cd

class CreateObatForm(forms.Form):
    id_merk_obat = forms.CharField(required=True)
    netto = forms.CharField(required=True)
    dosis = forms.CharField(required=True)
    aturan_pakai = forms.CharField(required=True)
    kontraindikasi = forms.CharField(required=True)
    bentuk_kesediaan = forms.CharField(required=True)

    def clean(self):
        cd = self.cleaned_data
        id_merk_obat = cd.get("id_merk_obat")
        netto = cd.get("netto")
        dosis = cd.get("dosis")
        aturan_pakai = cd.get("aturan_pakai")
        kontraindikasi = cd.get("kontraindikasi")
        bentuk_kesediaan = cd.get("bentuk_kesediaan")

        return cd



class createbalai(forms.Form):
    idbalai_field = forms.ChoiceField(choices = idbalai, label = 'ID Balai Pengobatan')
    jenisbalai_field = forms.ChoiceField(choices = jenisbalai, label = 'Jenis Balai Pengobatan')
