"""tk_basdat_43 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from apotek import views

app_name = 'apotek'
urlpatterns = [
    path('', views.list_apotek, name ='list_apotek'),
    path('produk/', views.list_produk_apotek, name ='list_produk_apotek'),
    path('delete/<id>', views.delete_apotek, name ='delete_apotek'),
    path('produk/delete/<id_apotek>/<id_produk>', views.delete_produk_apotek, name ='delete_produk_apotek'),
    path('update/<id>', views.update_apotek, name ='to_update_apotek'),
    path('produk/update/<id_apotek>/<id_produk>', views.update_produk_apotek,
         name ='to_update_produk_apotek'),
    path('create/', views.create_apotek, name ='create_apotek'),
    path('produk/create/', views.create_produk_apotek, name ='create_produk_apotek'),
    path('produk/avlble/<id>', views.id_produk_choices, name = 'id_produk_choice')
]
