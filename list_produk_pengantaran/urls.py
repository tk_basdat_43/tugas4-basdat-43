from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'list_produk_pengantaran'

urlpatterns = [
	path('list_produk_dibeli/',views.list_produk_dibeli,name='list_produk_dibeli'),
    path('create_list_produk_dibeli/',views.create_list_produk_dibeli,name='create_list_produk_dibeli'),
    path('pengantaran_farmasi/',views.pengantaran_farmasi,name='pengantaran_farmasi'),
    path('create_pengantaran_farmasi/',views.create_pengantaran_farmasi,name='create_pengantaran_farmasi'),
    path('delete/<id>', views.delete_pengantaran_farmasi, name ='delete_pengantaran_farmasi'),
    path('produk/delete/<id_apotek>/<id_produk>/<id_transaksi_pembelian>', views.delete_list_produk_dibeli, name ='delete_list_produk_dibeli'),
    path('update_pengantaran_farmasi/<id>', views.update_pengantaran_farmasi, name ='update_pengantaran_farmasi'),
    path('update_list_produk_dibeli/<id_apotek>/<id_produk>/<id_transaksi_pembelian>', views.update_list_produk_dibeli,
         name ='update_list_produk_dibeli'),
    path('apotek/avlble/<id>', views.id_apotek_choices, name = 'id_apotek_choice')
]
