from django.shortcuts import render, redirect
from django.db import connection, utils
from .forms import LoginForm, KurirForm, KonsumenForm, AlamatForm, AdminApotekForm, CSForm
from apotek.views import get_apotek_choices
from random import choice
from .decorators import *
# Create your views here.


def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


@no_login
def home(request):
    return render(request, 'landingpage.html')


@no_login
def login(request):
    with connection.cursor() as cursor:
        if request.method =='POST':
            form = LoginForm(request.POST)

            if form.is_valid():
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]

                cursor.execute("SELECT * FROM pengguna WHERE " +
                               "email = %s AND password = %s", [username, password])

                user = dictfetchall(cursor)

                if len(user) != 0:
                    user = user[0]
                    email = user["email"]
                    request.session["email"] = email
                    request.session["nama_lengkap"] = user["nama_lengkap"]
                    request.session["logged_in"] = True

                    if check_role(email, "konsumen"):
                        request.session["role"] = "KONSUMEN"

                    elif check_role(email, "admin_apotek"):
                        request.session["role"] = "ADMIN_APOTEK"

                    elif check_role(email, "kurir"):
                        request.session["role"] = "KURIR"

                    elif check_role(email, "cs"):
                        request.session["role"] = "CS"

                    print(dict(request.session))
                    return redirect("login_register:homepage")

                return redirect("login_register:login")
        else:
            form = LoginForm()
            args = {'form': form}
            return render(request, 'login.html', args)


@no_login
def register(request):
    return render(request, 'register.html')


@login_required
def homepage(request):
    return render(request, 'homepage.html')


@no_login
def kurir(request):
    form1 = KurirForm()

    if request.method == "GET":
        return render(request, 'kurir.html', {'form':form1})

    elif request.method == "POST":
        with connection.cursor() as cursor:
            form = KurirForm(request.POST)
            if form.is_valid():
                try:
                    create_pengguna(form)
                    email = form.cleaned_data.get("email")

                    cursor.execute("SELECT id_kurir FROM kurir ORDER BY id_kurir DESC LIMIT 1")
                    id_kurir = dictfetchall(cursor)[0]["id_kurir"]
                    id_kurir = str(int(id_kurir) + 1)

                    nama_perusahaan = form.cleaned_data.get("nama_perusahaan")

                    query = "INSERT INTO kurir(id_kurir, email, " \
                            "nama_perusahaan) " \
                            "VALUES(%s, %s, %s);"

                    cursor.execute(query, [id_kurir, email, nama_perusahaan])

                except utils.IntegrityError as e:
                    delete_user_table(email, "kurir")
                    delete_user_table(email, "pengguna")
                    return render(request, 'kurir.html',
                                  {'form': form1, "message": str(e)})

                return redirect("login_register:login")

            return render(request, 'kurir.html',
                          {'form': form1, "form1": form})


@no_login
def konsumen(request):
    form1 = KonsumenForm()
    form2 = AlamatForm()

    if request.method == "GET":
        return render(request, 'konsumen.html', {'form1':form1, 'form2':form2})

    elif request.method == "POST":
        with connection.cursor() as cursor:
            new_form1 = KonsumenForm(request.POST)
            new_form2 = AlamatForm(request.POST)
            if new_form1.is_valid() and new_form2.is_valid():
                try:
                    create_pengguna(new_form1)
                    email = new_form1.cleaned_data.get("email")

                    jenis_kelamin = new_form1.cleaned_data.get("jenis_kelamin")
                    tanggal_lahir = new_form1.cleaned_data.get("tanggal_lahir")
                    cursor.execute("SELECT id_konsumen FROM konsumen ORDER BY id_konsumen DESC LIMIT 1")
                    id_konsumen = dictfetchall(cursor)[0]["id_konsumen"]
                    id_konsumen = str(int(id_konsumen) + 1)

                    query = "INSERT INTO konsumen(id_konsumen, email, " \
                            "jenis_kelamin, tanggal_lahir) " \
                            "VALUES(%s, %s, %s, %s);"

                    cursor.execute(query, [id_konsumen, email, jenis_kelamin, tanggal_lahir])

                    alamat = new_form2.cleaned_data["alamat"]
                    status_alamat = new_form2.cleaned_data["status_alamat"]

                    query = "INSERT INTO alamat_konsumen(id_konsumen, alamat, " \
                            "status) " \
                            "VALUES(%s, %s, %s);"

                    for ii in range(len(alamat)):
                        cursor.execute(query, [id_konsumen, alamat[ii], status_alamat[ii]])

                except utils.IntegrityError as e:
                    delete_user_table(id_konsumen, "alamat_konsumen")
                    delete_user_table(email, "konsumen")
                    delete_user_table(email, "pengguna")
                    return render(request, "apotek/create_apotek.html",
                                  {'form1': form1, 'form2': form2,
                                   "message": str(e)})

                return redirect("login_register:login")

            return render(request,
                          'konsumen.html',
                          {'form1': form1, 'form2': form2,
                           'form1_errors': new_form1.errors,
                           'form2_errors': new_form2.errors})


@no_login
def admin_apotek(request):
    form1 = AdminApotekForm()
    if request.method == "GET":
        return render(request, 'admin_apotek.html', {'form':form1})

    elif request.method == "POST":
        with connection.cursor() as cursor:
            form = AdminApotekForm(request.POST)

            if form.is_valid():
                try:
                    create_pengguna(form)
                    email = form.cleaned_data.get("email")

                    query = "INSERT INTO apoteker(email)" \
                            "VALUES(%s);"
                    cursor.execute(query, [email])

                    list_id_apotek = get_apotek_choices()
                    id_apotek = choice(list_id_apotek)

                    query = "INSERT INTO admin_apotek(email, id_apotek)" \
                            "VALUES(%s, %s);"

                    cursor.execute(query, [email, id_apotek])

                except utils.IntegrityError as e:
                    delete_user_table(email, "admin_apotek")
                    delete_user_table(email, "apoteker")
                    delete_user_table(email, "pengguna")
                    return render(request, 'admin_apotek.html',
                                  {'form': form1, "message": str(e)})

                return redirect("login_register:login")

            return render(request, 'admin_apotek.html',
                          {'form':form1, "form1": form})


@no_login
def cs(request):
    form1 = CSForm()
    if request.method == "GET":
        return render(request, 'CS.html', {'form':form1})

    elif request.method == "POST":
        form = CSForm(request.POST)
        with connection.cursor() as cursor:

            if form.is_valid():
                try:
                    create_pengguna(form)
                    email = form.cleaned_data.get("email")

                    query = "INSERT INTO apoteker(email)" \
                            "VALUES(%s);"
                    cursor.execute(query, [email])

                    no_ktp = form.cleaned_data.get("no_ktp")
                    no_sia = form.cleaned_data.get("no_sia")

                    query = "INSERT INTO cs(no_ktp, email, no_sia)" \
                            "VALUES(%s, %s, %s);"

                    cursor.execute(query, [no_ktp, email, no_sia])

                except utils.IntegrityError as e:
                    delete_user_table(email, "cs")
                    delete_user_table(email, "apoteker")
                    delete_user_table(email, "pengguna")
                    return render(request, 'CS.html',
                                  {'form': form1, "message": str(e)})

                return redirect("login_register:login")

            return render(request, 'CS.html',
                          {'form': form1, "form1": form})


def check_role(id, role):
    with connection.cursor() as cursor:
        query = "SELECT * FROM " + role + " WHERE email = %s"
        cursor.execute(query, [id])

        row = dictfetchall(cursor)

        if len(row) != 0:
            return True
        else:
            return False


def delete_user_table(id, table):
    with connection.cursor() as cursor:
        query = ""
        if table == "alamat_konsumen":
            query = "DELETE FROM alamat_konsumen WHERE id_konsumen = %s"
        else:
            query = "DELETE FROM " + table + " WHERE email = %s"

        cursor.execute(query, [id])


def create_pengguna(form):
    with connection.cursor() as cursor:
        cd = form.cleaned_data
        email = cd.get("email")
        password = cd.get("password")
        nama_lengkap = cd.get("nama_lengkap")
        no_telepon = cd.get("no_telepon")

        query = "INSERT INTO pengguna(email, telepon, " \
                "password, nama_lengkap) " \
                "VALUES(%s, %s, %s, %s);"

        cursor.execute(query, [email, no_telepon, password, nama_lengkap])


@login_required
def logout(request):
    request.session.flush()
    return redirect("login_register:landingpage")
