import pandas as pd
import os
from django.conf import settings

class DataApotek:
    link_apotek = os.path.join(settings.BASE_DIR, 'data_csv/APOTEK.csv')
    link_produk_apotek = os.path.join(settings.BASE_DIR, 'data_csv/PRODUK_APOTEK.csv')
    data_apotek = pd.read_csv(link_apotek).astype(str)
    data_produk_apotek = pd.read_csv(link_produk_apotek)\
        .astype({"id_apotek":str, "id_produk":str})